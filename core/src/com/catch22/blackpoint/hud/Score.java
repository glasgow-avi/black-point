package com.catch22.blackpoint.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.catch22.blackpoint.gui.Game;

public class Score
{
    public static int score1, score2;
    private static OrthographicCamera camera;
    private static BitmapFont font;
    private static FreeTypeFontGenerator generator;
    private static FreeTypeFontGenerator.FreeTypeFontParameter parameter;

    public Score()
    {
        generator = new FreeTypeFontGenerator(Gdx.files.internal("score.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 64;
        parameter.color = Color.WHITE;

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        font = generator.generateFont(parameter);

        score1 = 0;
        score2 = 0;
    }

    public static void render()
    {
        Game.batch.setProjectionMatrix(camera.combined);
        Game.batch.begin();
        font.setColor(Color.WHITE);
        font.draw(Game.batch, score1 + " - " + score2, -60, camera.viewportHeight / 2 - 20);
        Game.batch.end();
        Game.batch.setProjectionMatrix(Game.camera.combined);
    }
}