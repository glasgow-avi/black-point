package com.catch22.blackpoint.walls;

import com.badlogic.gdx.math.Vector2;
import com.catch22.blackpoint.hud.Score;
import com.catch22.blackpoint.sprite.Ball;

public class End extends com.catch22.blackpoint.walls.Bounds
{
    public End(Vector2... args)
    {
        super(args);
    }

    @Override
    public void hit()
    {
        if(Ball.body.getPosition().x < 0)
            Score.score2++;
        else
            Score.score1++;
    }
}