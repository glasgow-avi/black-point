package com.catch22.blackpoint.walls;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ContactListener implements com.badlogic.gdx.physics.box2d.ContactListener
{
    @Override
    public void beginContact(Contact contact)
    {
        Fixture a = contact.getFixtureA(), b = contact.getFixtureB();

        if(a.getUserData() == "ball" || b.getUserData() == "ball")
        {
            Fixture ball = a.getUserData() == "ball" ? a : b, object = a.getUserData() == "ball" ? b : a;

            if(object.getUserData() != null && com.catch22.blackpoint.walls.Bounds.class.isAssignableFrom(object.getUserData().getClass()))
                ((com.catch22.blackpoint.walls.Bounds) object.getUserData()).hit();
        }
    }

    @Override
    public void endContact(Contact contact)
    {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold)
    {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse)
    {

    }

}
