package com.catch22.blackpoint.walls;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.catch22.blackpoint.gui.Game;

public abstract class Bounds
{
    public Bounds(Vector2... args)
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0, 0);

        ChainShape chain = new ChainShape();
        chain.createChain(args);

        FixtureDef fDef = new FixtureDef();
        fDef.shape = chain;
        fDef.friction = 0;
        fDef.restitution = 1;

        Game.world.createBody(bodyDef).createFixture(fDef).setUserData(this);
    }

    public abstract void hit();
}