package com.catch22.blackpoint;

import com.catch22.blackpoint.gui.Game;

public class BlackPoint extends com.badlogic.gdx.Game
{
	public static final float scale = 10;

	@Override
	public void create ()
	{
		setScreen(new Game());
	}
}
