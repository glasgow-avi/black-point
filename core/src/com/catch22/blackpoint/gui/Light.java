package com.catch22.blackpoint.gui;

import com.badlogic.gdx.graphics.Color;
import box2dLight.PointLight;
import box2dLight.RayHandler;

public class Light extends PointLight
{
    public static RayHandler rayhandler = new RayHandler(Game.world);

    public Light()
    {
        super(rayhandler, 100, Color.WHITE, 6, 0, 0);
        rayhandler.setAmbientLight(.1f);
        setDistance(Game.camera.viewportWidth / 4);
    }
}
