package com.catch22.blackpoint.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.blackpoint.BlackPoint;
import com.catch22.blackpoint.hud.Score;
import com.catch22.blackpoint.sprite.Ball;
import com.catch22.blackpoint.walls.ContactListener;
import com.catch22.blackpoint.walls.End;
import com.catch22.blackpoint.sprite.Paddle;
import com.catch22.blackpoint.walls.Wall;

public class Game implements Screen
{
    public static final SpriteBatch batch = new SpriteBatch();
    public static OrthographicCamera camera;

    private static Box2DDebugRenderer debugRenderer;

    public static World world;

    private Paddle player1, player2;

    private static final float frameRate = 1 / 60f;
    private static final int velocityStep = 8, positionIterations = 3;

    public Game()
    {
        new Score();

        world = new World(new Vector2(0, 0), true);
        camera = new OrthographicCamera(Gdx.graphics.getWidth() / BlackPoint.scale, Gdx.graphics.getHeight() / BlackPoint.scale);

        world.setContactListener(new ContactListener());

        debugRenderer = new Box2DDebugRenderer();
    }

    @Override
    public void show()
    {
        new Wall(new Vector2(-Game.camera.viewportWidth / 2, Game.camera.viewportHeight / 2), new Vector2(Game.camera.viewportWidth / 2, Game.camera.viewportHeight / 2));
        new End(new Vector2(-Game.camera.viewportWidth / 2, Game.camera.viewportHeight / 2), new Vector2(-Game.camera.viewportWidth / 2, -Game.camera.viewportHeight / 2));
        new Wall(new Vector2(-Game.camera.viewportWidth / 2, -Game.camera.viewportHeight / 2), new Vector2(Game.camera.viewportWidth / 2, -Game.camera.viewportHeight / 2));
        new End(new Vector2(Game.camera.viewportWidth / 2, Game.camera.viewportHeight / 2), new Vector2(Game.camera.viewportWidth / 2, -Game.camera.viewportHeight / 2));

        player1 = new Paddle(true);
        player2 = new Paddle(false);
        new Ball();

        new Light().attachToBody(Ball.body);
    }

    @Override
    public void render(float delta)
    {
        update(delta);

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        Light.rayhandler.setCombinedMatrix(camera.combined);

        batch.begin();
//        {
//            player1.render();
//            player2.render();
//        }
        batch.end();

//        debugRenderer.render(world, camera.combined);
        Light.rayhandler.render();

        Score.render();
    }

    private void handleInput()
    {
        if(Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isTouched() && Gdx.input.getX() > 2 * Gdx.graphics.getWidth() / 3 && Gdx.input.getY() < Gdx.graphics.getHeight() / 3)
            player2.move(true);
        else if(Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isTouched() && Gdx.input.getX() > 2 * Gdx.graphics.getWidth() / 3 && Gdx.input.getY() > 2 * Gdx.graphics.getHeight() / 3)
            player2.move(false);
        else
            player2.halt();
        if(Gdx.input.isKeyPressed(Input.Keys.Q) || Gdx.input.isTouched() && Gdx.input.getX() < Gdx.graphics.getWidth() / 3 && Gdx.input.getY() < Gdx.graphics.getHeight() / 3)
            player1.move(true);
        else if(Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isTouched() && Gdx.input.getX() < Gdx.graphics.getWidth() / 3 && Gdx.input.getY() > 2 * Gdx.graphics.getHeight() / 3)
            player1.move(false);
        else
            player1.halt();
    }

    private void update(float delta)
    {
        handleInput();

        player1.update();
        player2.update();

        Light.rayhandler.update();

        world.step(frameRate, velocityStep, positionIterations);
    }

    @Override
    public void resize(int width, int height)
    {
        
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        
    }
}