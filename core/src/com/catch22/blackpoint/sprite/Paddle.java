package com.catch22.blackpoint.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.catch22.blackpoint.BlackPoint;
import com.catch22.blackpoint.gui.Game;

public class Paddle extends Sprite
{
    public static final float width = 14f / BlackPoint.scale, height = 40f / BlackPoint.scale;
    public Body body;
    
    private static final Texture texture = new Texture("paddle.png");

    private static final float velocity = 400f / BlackPoint.scale;

    public Paddle(boolean playerId)
    {
        super(texture);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set((-Game.camera.viewportWidth / 2 + width / 2) * (playerId ? 1 : -1), 0);

        body = Game.world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);

        Game.world.createBody(bodyDef);

        FixtureDef fDef = new FixtureDef();
        fDef.friction = 0;
        fDef.shape = shape;
        fDef.restitution = 2f;

        body.createFixture(fDef);

        setSize(2 * width, 2 * height);
        body.setUserData(this);
    }

    public void render()
    {
        ((Sprite) body.getUserData()).draw(Game.batch);
    }

    public void update()
    {
        ((Sprite) body.getUserData()).setPosition(this.body.getPosition().x - width, this.body.getPosition().y - height);
    }

    public void halt()
    {
        body.setLinearVelocity(0, 0);
    }

    public void move(boolean direction)
    {
        body.setLinearVelocity(0, (direction ? 1 : -1) * velocity);
    }
}
