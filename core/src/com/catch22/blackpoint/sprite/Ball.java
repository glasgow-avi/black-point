package com.catch22.blackpoint.sprite;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.catch22.blackpoint.BlackPoint;
import com.catch22.blackpoint.gui.Game;

public class Ball extends Sprite
{
    public static Body body;

    public static final float radius = 0.1f;

    public Ball()
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(0, 0);

        body = Game.world.createBody(bodyDef);


        CircleShape shape = new CircleShape();
        shape.setRadius(BlackPoint.scale * radius);

        Game.world.createBody(bodyDef);

        FixtureDef fDef = new FixtureDef();
        fDef.density = 1;
        fDef.friction = 0;
        fDef.shape = shape;
        fDef.restitution = 1f;

        body.createFixture(fDef).setUserData("ball");

        body.setLinearVelocity(200f / BlackPoint.scale, 200f / BlackPoint.scale);

        new Thread()
        {
            @Override
            public void run()
            {
                while(true)
                {
                    try
                    {
                        sleep(10);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    Vector2 velocity = body.getLinearVelocity();
                    if(velocity.x > velocity.y)
                        body.setLinearVelocity(velocity.x, velocity.y / Math.abs(velocity.y) * Math.abs(velocity.x));
                    else
                        body.setLinearVelocity(velocity.x / Math.abs(velocity.x) * Math.abs(velocity.y), velocity.y);

                    if(Math.abs(body.getLinearVelocity().x) < 100f / BlackPoint.scale)
                        body.applyLinearImpulse(new Vector2(500f * body.getLinearVelocity().x / Math.abs(body.getLinearVelocity().x) / BlackPoint.scale, 500f * body.getLinearVelocity().y / Math.abs(body.getLinearVelocity().y) / BlackPoint.scale), body.getWorldCenter(), true);
                }
            }
        }.start();
    }
}